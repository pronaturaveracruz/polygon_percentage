```
# polygon_percentage

Disecta un polígono en porcentajes definidos por el usuario, para generar visualizaciones
library(units)
library(sf)
library(dplyr)
library(osmdata)
#library(ggplot2)
#library(ggsci)

xalver <- osmdata::getbb("veracruz", format_out = "sf_polygon")
xalver <- xalver2
plot(xalver)
porcientos <- c(.6,.3, .1)

function(poly, porcentajes) {}
df   <- st_sf(id = 1:length(porcientos), crs = 4326, # empty sf for populating
              geometry = st_sfc(lapply(1:length(porcientos), function(x) st_multipolygon())))
area1   <- st_area(xalver)  %>% sum() # check multipolygons; # area1 is constant
xalver2 <- xalver # maybe duplicating
for(j in  seq_along(porcientos[-length(porcientos)])) { 
  bb = st_bbox(xalver)
  top <- bb['ymax']
  bot <- bb['ymin']
  steps <- seq(bot, top, by = (top - bot) / 80)
  for(i in steps[2:length(steps)]) {  # 2:n because 1:n renders a line "ymax" = "ymin"
    bf <- bb
    bf['ymax'] = i
    temp <- st_intersection(xalver, st_as_sfc(bf, 4326))
    area2 <- st_area(temp) %>% sum()           # con get(.., i) coz st_area prints rounded
    if(drop_units(area2)/drop_units(area1) >= porcientos[j]) break
    df$geometry[j] <- st_geometry(temp)
  }
  xalver <- st_difference(xalver, st_union(df))
}
df$geometry[length(porcientos)] <- st_geometry(st_difference(xalver2, st_union(df)))
df$valor <- rev(c("Primario: 8%", "Secundario: 32%", "Terciario: 60%"))

st_area(df)/area1
st_bbox(df) <- st_union(df)

ggplot(df) + geom_sf(aes(fill = as.factor(id))) + 
  coord_sf(crs = st_crs(df), datum = NA) +
  scale_fill_simpsons(guide = F) + geom_sf_label(aes(label = valor), size = 3) + 
  theme(
    panel.ontop = TRUE,   ## Note: this is to make the panel grid visible in this example
    panel.grid = element_blank(), 
    line = element_blank(), 
    rect = element_blank()) + 
  labs(title = "PIB por Sector, Veracruz", caption = "pronaturaveracruz.org") + 
  xlab("") + ylab("")
ggsave("sectores_ver.png", width = 6, height = 4)
```